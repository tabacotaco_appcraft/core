import React, { useRef, useState, useMemo, useCallback } from 'react';
import PropTypes, { InferType } from 'prop-types';

import DateFnsUtils from '@date-io/date-fns';
import cx from 'clsx';

import _get from 'lodash/get';
import _omit from 'lodash/omit';
import _pick from 'lodash/pick';
import _set from 'lodash/set';

import Container from '@material-ui/core/Container';
import LinearProgress from '@material-ui/core/LinearProgress';
import MuiPickersUtilsProvider from '@material-ui/pickers/MuiPickersUtilsProvider';
import { makeStyles } from '@material-ui/core/styles';

import DesktopAccessDisabledIcon from '@material-ui/icons/DesktopAccessDisabled';

import { AppcraftParser } from '@appcraft/parser';

import WidgetBase from './widget';
import { WidgetProvider, useGlobalState, useSubstratumWidgets, useVisualizerReady } from './_customs';


//* Definitions
const PROP_TYPES = {
  IMPL: {
    lazyDeps: PropTypes.array,
    ready: PropTypes.arrayOf(PropTypes.object)
  },
  MAIN: {
    children: PropTypes.node.isRequired,
    proxy: PropTypes.objectOf(PropTypes.elementType),
    ready: PropTypes.arrayOf(PropTypes.object),

    definitions: PropTypes.exact({
      decorations: PropTypes.objectOf(
        PropTypes.exact({
          configTypes: PropTypes.object,
          defaultProps: PropTypes.object,
          description: PropTypes.string,
          propTypes: PropTypes.object
        })
      ),
      props: PropTypes.objectOf(
        PropTypes.exact({
          defaultProps: PropTypes.object,
          description: PropTypes.string,
          propTypes: PropTypes.object
        })
      )
    }),
    state: PropTypes.objectOf(
      PropTypes.exact({
        path: PropTypes.string.isRequired,
        typeId: PropTypes.string.isRequired,
        defaultValue: PropTypes.any
      })
    ),
    widgets: PropTypes.arrayOf(
      PropTypes.exact({
        description: PropTypes.string,
        handles: PropTypes.arrayOf(PropTypes.object),
        importBy: PropTypes.string,
        index: PropTypes.number,
        props: PropTypes.object,
        superior: PropTypes.string,
        typePairs: PropTypes.objectOf(PropTypes.string),
        uid: PropTypes.string.isRequired,

        decoration: PropTypes.arrayOf(
          PropTypes.exact({
            description: PropTypes.string,
            importBy: PropTypes.string,
            options: PropTypes.object,
            typePairs: PropTypes.objectOf(PropTypes.string),
            uid: PropTypes.string.isRequired
          })
        )
      })
    )
  }
};

namespace AppcraftVisualizer {
  export namespace def {
    enum BaseTypeName { Array, Boolean, Date, Number, Object, String };
    enum RefTypeName { input, state, todo };

    export type StateBinding = { path: string; typeId: string; defaultValue?: any; };
    type VariableType = keyof typeof BaseTypeName | keyof typeof RefTypeName;

    interface Variable<T extends VariableType = VariableType> {
      uid: string;
      type: T;
      finalType?: BaseTypeName;
      description: string;
      initValue?: Variable[] | Record<string, Variable> | string;
      treatments?: ({
        uid: string;
        description: string;
        name: string;
        args?: Variable[];
      })[];
    };

    type Condition = { uid: string; description: string; source: Variable; value: Variable; };

    interface HandleBase {
      type: string;
      uid: string;
      description: string;
      state?: string;
      condition?: Condition[];
    };

    interface RequestHandle extends HandleBase {
      type: 'request';
      url: string;
      method: 'DELETE' | 'GET' | 'HEAD' | 'OPTIONS' | 'PATCH' | 'POST' | 'PUT';
      header?: Record<string, string>;
      search?: Record<string, Variable>;
      body?: Variable<'Object'>;
    };

    interface CalculatorHandle extends HandleBase {
      type: 'calculator';
      params: Variable[];
      template: string;
    };

    interface MapHandle extends HandleBase {
      type: 'map';
      mappable?: Condition[];
      source: Variable<'Array'>[];
      pairs: (Omit<CalculatorHandle, 'condition'> & { path: string; })[];
    };

    interface OptionsBase {
      description?: string;
      importBy?: string;
      typePairs?: Record<string, string>;
      uid: string;
    }

    export interface WidgetOptions extends OptionsBase {
      decoration?: (OptionsBase & { options: Record<string, any> })[];
      handles: Record<string, (RequestHandle | CalculatorHandle | MapHandle)[]>;
      index: number;
      props: Record<string, any>;
      superior?: string;
    };

    export interface ImplementProps extends Pick<PropTypes.InferProps<typeof PROP_TYPES.IMPL>, 'lazyDeps'> {
      ready?: (RequestHandle | CalculatorHandle | MapHandle)[];
    }

    export interface MainProps extends Pick<PropTypes.InferProps<typeof PROP_TYPES.MAIN>, 'children' | 'proxy'> {
      ready?: ImplementProps['ready'];
      state?: Record<string, StateBinding[]>;
      widgets: WidgetOptions[];

      definitions?: {
        decorations?: Record<string, {
          description?: string;
          propTypes?: AppcraftParser.def.PropDefinition<'arrayOf'>;
          configTypes?: AppcraftParser.def.PropDefinition<'arrayOf'>;
          defaultProps?: Record<string, any>;
        }>;

        props?: Record<string, {
          description?: string;
          propTypes: AppcraftParser.def.PropDefinition<'exact'>;
          defaultProps?: Record<string, any>;
        }>;
      };
    };
  }
}

export type DefinitionsType = AppcraftVisualizer.def.MainProps['definitions'];
export type GlobalStateType = AppcraftVisualizer.def.MainProps['state'];
export type ReadyTodoType = AppcraftVisualizer.def.MainProps['ready'];
export type StateBindingType = AppcraftVisualizer.def.StateBinding;
export type WidgetOptionsType = AppcraftVisualizer.def.WidgetOptions;


//* Custom Hooks
// @ts-ignore
const useWidgetProviderValues = (definitions, proxy, defaultState, widgets) => {
  const [disabledProps, setDisabledProps] = useState(new Map());
  const [listeners, setListeners] = useState([]);
  const [state, onStateChange] = useGlobalState(defaultState);

  return useMemo(() => ({
    definitions,
    disabledProps,
    listeners,
    proxy,
    widgets,
    state,

    // @ts-ignore
    onListenersActived: (e) => setListeners(e === false ? [] : e),
    // @ts-ignore
    onPropsDisable: (locked) => setDisabledProps(new Map(Object.entries(locked))),
    onStateChange
  }), [disabledProps, listeners, state, widgets]);
};

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'block',
    border: 0,
    overflow: 'hidden auto !important',
    padding: theme.spacing(1, 0),
    height: '100%',

    '& > * + *': {
      marginTop: theme.spacing(1.5)
    },

    '&.empty': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',

      '& > svg': {
        fontSize: '20vh',
        opacity: theme.palette.action.focusOpacity,
        color: theme.palette.info.light
      }
    }
  }
}));


//* Component
export const WidgetImplement: React.FC<AppcraftVisualizer.def.ImplementProps> = ({ lazyDeps = [], ready }) => {
  const { children: substratum } = useSubstratumWidgets() as Record<string, AppcraftVisualizer.def.WidgetOptions[]>;

  const container = useRef<HTMLDivElement>();
  const onReady = useVisualizerReady(ready);
  const classes = useStyles();

  const LazyWidgetBase = useMemo(() => (
    React.lazy(() => (
      onReady().then(() => ({
        default: WidgetBase
      }))
    ))
  ), lazyDeps);

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <React.Suspense fallback={(<LinearProgress />)}>
        <Container role="AppcraftVisualizer" ref={container} disableGutters maxWidth={false} className={cx(classes.root, { empty: !Boolean(substratum) })}>
          {!Boolean(substratum) && (
            <DesktopAccessDisabledIcon />
          )}

          <LazyWidgetBase />
        </Container>
      </React.Suspense>
    </MuiPickersUtilsProvider>
  );
};

export const WidgetWrapper: React.FC<AppcraftVisualizer.def.MainProps> = ({ children, definitions, proxy, state: defaultState, widgets }) => {
  const providerValues = useWidgetProviderValues(definitions, proxy, defaultState, widgets);

  return ( // @ts-ignore
    <WidgetProvider value={providerValues}>
      {children}
    </WidgetProvider>
  );
};

// @ts-ignore
WidgetWrapper.propTypes = PROP_TYPES.MAIN;
WidgetWrapper.displayName = 'WidgetWrapper';

const Visualizer: React.FC<Pick<AppcraftVisualizer.def.MainProps, 'proxy' | 'ready' | 'state' | 'widgets'>> = ({ ready, ...props }) => (
  <WidgetWrapper {...props}>
    <WidgetImplement ready={ready} />
  </WidgetWrapper>
);

// @ts-ignore
Visualizer.propTypes = _pick(PROP_TYPES.MAIN, ['proxy', 'ready', 'state', 'widgets']);
Visualizer.displayName = 'AppcraftVisualizer';

export default Visualizer;
