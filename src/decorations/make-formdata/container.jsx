import React, { createContext, useContext } from 'react';
import PropTypes from 'prop-types';

//* Custom Hooks
const FormDataContext = createContext({
  data: null,
  onChange: () => null
});

export const useFormData = () => useContext(FormDataContext);


//* HOC
const makeFormContainer = () => (
  (ContainerElement) => {
    const FormContainer = ({ FormContainerProps, ...implementProps }) => (
      <FormDataContext.Provider value={FormContainerProps}>
        <ContainerElement {...implementProps} />
      </FormDataContext.Provider>
    );

    FormContainer.Naked = ContainerElement;
    FormContainer.displayName = 'FormContainer';
    FormContainer.overrideds = [];

    return FormContainer;
  }
);

makeFormContainer.configTypes = {};

makeFormContainer.propTypes = {
  FormContainerProps: PropTypes.exact({
    data: PropTypes.object,
    onChange: PropTypes.func
  })
};

export default makeFormContainer;
