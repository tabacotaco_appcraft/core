import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import _get from 'lodash/get';
import _merge from 'lodash/merge';
import _omit from 'lodash/omit';
import _set from 'lodash/set';
import _toPath from 'lodash/toPath';

import { useFormData } from './container';


const makeFormField = ({ valueSource, targetProp, handleChangeProp, handleValueExtraPath }) => {
  const sourcePath = _toPath(valueSource);
  const targetPath = _toPath(targetProp);
  const handlePath = _toPath(handleChangeProp);

  return (FieldElement) => {
    const FormField = (implementProps) => {
      const { data, onChange } = useFormData();
      const props = _omit(implementProps, [targetPath, handlePath]);

      const overrideds = useMemo(() => {
        const result = {};

        _set(result, targetPath, _get(data, sourcePath) || null);

        _set(result, handleValueExtraPath, (...e) => {
          const handleChange = _get(implementProps, handlePath);

          handleChange?.(...e);
          onChange(_set({ ...data }, targetPath, _get(e, handleValueExtraPath) || null));
        });

        return result;
      }, [data, onChange, _get(implementProps, handlePath)]);

      return (
        <FieldElement {..._merge(props, overrideds)} />
      );
    };

    FormField.Naked = FieldElement;
    FormField.displayName = 'FormField';
    FormField.overrideds = [targetProp, handleChangeProp].filter((path) => path?.trim());

    return FormField;
  };
};

makeFormField.configTypes = {
  changePath: PropTypes.string,
  handleValueExtraPath: PropTypes.string,
  valueSource: PropTypes.string.isRequired,
  targetProp: PropTypes.string.isRequired
};

makeFormField.propTypes = {};

export default makeFormField;
