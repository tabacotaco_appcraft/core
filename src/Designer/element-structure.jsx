/* eslint-disable react/prop-types */
import React, { createContext, useCallback, useContext, useEffect, useImperativeHandle, useMemo, useReducer, useRef, useState } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';

import AppBar from '@material-ui/core/AppBar';
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Switch from '@material-ui/core/Switch';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import AddIcon from '@material-ui/icons/Add';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CloseIcon from '@material-ui/icons/Close';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import QueuePlayNextIcon from '@material-ui/icons/QueuePlayNext';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';

import _get from 'lodash/get';
import _set from 'lodash/set';
import _toPath from 'lodash/toPath';
import cx from 'clsx';

import IconMenuButton, { IconMenuItem } from './icon-menu-button';
import { getClosetElements } from './_customs';
import { useLocales } from '../_utils/locales';
import { useSubstratumWidgets, useWidgetContext, getSubstratumWidgets } from '../Visualizer/_customs';


const ROOT_ID = '$root';

function isAppendable(importBy, definitions, substratums) {
  const childrenType = _get(definitions, ['props', importBy, 'propTypes', 'options', 'children', 'type']);
  const subCount = substratums.children?.length || 0;

  return importBy && (childrenType === 'node' || (childrenType === 'element' && subCount < 1));
}

function isCombineWithChild(widgets, draggableId, droppableId) {
  const { superior = null } = widgets.find(({ uid }) => uid === droppableId) || {};
  const [uid = null] = _toPath(superior);

  return uid === draggableId || (uid ? isCombineWithChild(widgets, draggableId, uid) : false);
}


//* Custom Hooks
const NodeContext = createContext({
  container: () => [],
  dnd: null,
  expandeds: new Set(),

  onActived: () => null,
  onAppend: () => null,
  onDestroy: () => null,
  onDnd: () => null,
  onElementChange: () => null,
  onModify: () => null
});

const useDnd = (() => {
  function reducer(state, action) {
    return (Array.isArray(action) ? action : [action]).reduce(
      (prevState, { type, value }) => {
        switch (type) {
          case 'done':
            return { ...prevState, drag: null, drop: null };

          case 'switch':
            return { ...prevState, enabled: value === true };

          case 'drag': {
            if (prevState.enabled) {
              return { ...prevState, drag: value };
            }

            break;
          }
          case 'drop': {
            if (prevState.enabled) {
              return { ...prevState, drop: value };
            }

            break;
          }
          default:
        }

        return prevState;
      },
      state
    );
  }

  return (widgets, onSort) => {
    const { definitions } = useWidgetContext();
    const [dnd, dispatch] = useReducer(reducer, { drag: null, drop: null, enabled: false });
    const { drag, drop } = dnd;

    return [
      dnd,
      dispatch,

      {
        start: useCallback(({ draggableId, source }) => {
          const { superior } = widgets[source.index];

          dispatch([
            { type: 'drag', value: draggableId },
            { type: 'drop', value: superior || ROOT_ID }
          ]);
        }, [widgets]),

        update: useCallback(({ destination, combine }) => {
          if (combine) {
            const { superior, importBy } = widgets.find(({ uid }) => uid === combine.draggableId);

            if (isAppendable(importBy, definitions, getSubstratumWidgets(widgets, combine.draggableId, false))) {
              dispatch({ type: 'drop', value: `${combine.draggableId}.children` });
            } else {
              dispatch({ type: 'drop', value: superior || ROOT_ID });
            }
          } else if (destination) {
            const { superior } = widgets[destination.index];

            dispatch({ type: 'drop', value: superior || ROOT_ID });
          }
        }, [widgets, definitions]),

        end: useCallback(({ source, combine, destination }) => {
          if (!isCombineWithChild(widgets, drag, _toPath(drop)[0])) {
            if (combine && isAppendable(
              widgets.find(({ uid }) => uid === combine.draggableId).importBy,
              definitions,
              getSubstratumWidgets(widgets, combine.draggableId, false)
            )) {
              onSort(widgets.map((widget) => ({ ...widget, ...(widget.uid === drag && { superior: drop }) })));
            } else if (destination && destination.index !== source.index) {
              const [fm, to] = [Math.min(source.index, destination.index), Math.max(source.index, destination.index) + 1];
              const modifieds = widgets.slice(fm, to);
              const behind = source.index < destination.index;
              const target = modifieds[behind ? 'shift' : 'pop']();

              modifieds[behind ? 'push' : 'unshift'](target);

              onSort([
                ...widgets.slice(0, fm),
                ...modifieds.map((widget, i) => ({
                  ...widget,
                  index: fm + i,
                  ...(widget.uid === drag && {
                    superior: drop === ROOT_ID
                      ? null
                      : getClosetElements(widgets, widgets[destination.index].uid).includes(target.uid)
                        ? target.superior
                        : drop
                  })
                })),
                ...widgets.slice(to)
              ]);
            }
          }

          dispatch({ type: 'done' });
        }, [drag, drop, widgets, definitions])
      }
    ];
  };
})();

const useNodeProviderValue = (appbarRef, dnd, onDnd, onActived, onAppend, onDestroy, onModify) => {
  const [expandeds, setExpandeds] = useState(new Set());

  return [
    useMemo(() => ({
      container: () => Array.from(appbarRef.current.nextSibling.querySelectorAll('li[data-widget]')).map((el) => el.dataset.widget),
      dnd,
      expandeds,

      onActived,
      onAppend,
      onDestroy,
      onDnd,
      onModify,

      onElementChange: (uid, isExpanded) => {
        expandeds[isExpanded ? 'add' : 'delete'](uid);
        setExpandeds(new Set(expandeds));
      }
    }), [expandeds, dnd]),

    setExpandeds
  ];
};

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    overflow: ({ level } = {}) => `hidden ${!level ? 'auto' : 'hidden'} !important`,
    width: '100%',
    height: ({ level } = {}) => (!level ? '100%' : 'fit-content')
  },
  appbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,

    '& > div': {
      '& > *:first-child': {
        marginRight: 'auto'
      },
      '& > hr': {
        margin: theme.spacing(0, 1)
      }
    }
  },
  item: {
    minHeight: theme.spacing(6),
    paddingLeft: ({ level = 0 } = {}) => theme.spacing(level * 1.5),

    '& div[role=icon]': {
      minWidth: theme.spacing(4)
    },
    '&.dragging': {
      background: theme.palette.background.paper,
      border: `1px solid ${theme.palette.divider}`,
      borderRadius: theme.shape.borderRadius,
      opacity: 0.6
    },
    '& > div[role=action]': {
      alignItems: 'center',
      display: 'flex',
      justifyContent: 'center',
      height: '100%',

      '& > button, & > *[role=dnd]': {
        opacity: theme.palette.action.disabledOpacity,

        '&:hover': {
          opacity: 1
        }
      }
    },
    '& + .entered': {
      height: 'fit-content !important',
      minHeight: 'fit-content !important'
    }
  }
}));


//* Components
function ElementItem({ prefix, level = 0, widget }) {
  const { superior, uid, index, importBy, description } = widget;
  const { getFixedT: dt } = useLocales();
  const { definitions } = useWidgetContext();
  const { container, dnd, expandeds, onActived, onAppend, onDestroy, onModify, onElementChange } = useContext(NodeContext);

  const substratums = useSubstratumWidgets({ superior: uid, stringify: false });
  const appendable = isAppendable(importBy, definitions, substratums);

  const dragging = Boolean(dnd.drag);
  const draggable = Boolean(dnd.enabled && (!superior || superior.endsWith('.children')));
  const isExpanded = expandeds.has(uid) || dnd.enabled;
  const classes = useStyles({ level });

  useEffect(() => {
    const i = container().indexOf(uid);

    if (i !== index) {
      onModify({ ...widget, index: i });
    }
  }, [widget]);

  return (
    <>
      <Draggable index={index} draggableId={uid} isDragDisabled={!draggable}>
        {(dragProv) => (
          <ListItem
            button
            classes={{ container: cx(classes.item, { dragging: dnd.drag === uid }) }}
            onClick={() => onActived(uid)}
            ContainerProps={{ ...dragProv.draggableProps, ref: dragProv.innerRef, 'data-widget': uid }}
          >
            <ListItemIcon role="icon" onClick={(e) => e.stopPropagation()}>
              {Object.keys(substratums).length > 0 && (
                <IconButton size="small" onClick={() => onElementChange(uid, !isExpanded)}>
                  {isExpanded
                    ? (<ExpandMoreIcon />)
                    : (<ChevronRightIcon />)}
                </IconButton>
              )}
            </ListItemIcon>

            <ListItemText
              {...(prefix
                ? { primary: prefix, secondary: description }
                : { primary: description }
              )}
            />

            <ListItemSecondaryAction {...dragProv.dragHandleProps} role="action">
              {!dnd.enabled && (
                <IconMenuButton size="small" color="primary" icon={(<SettingsOutlinedIcon />)}>
                  {appendable && (
                    <IconMenuItem
                      icon={(<AddIcon color="primary" />)}
                      text={dt('btn-create-element')}
                      onClick={() => {
                        onElementChange(uid, true);
                        onAppend({ target: uid, value: 'children' });
                      }}
                    />
                  )}

                  <IconMenuItem icon={(<CloseIcon color="secondary" />)} text={dt('btn-remove-element')} onClick={() => onDestroy(uid)} />
                </IconMenuButton>
              )}

              {dnd.enabled && draggable && (!dragging || dnd.drag === uid) && (
                <ImportExportIcon role="dnd" />
              )}
            </ListItemSecondaryAction>
          </ListItem>
        )}
      </Draggable>

      <Collapse in={isExpanded} timeout="auto" classes={{ entered: 'entered' }}>
        {isExpanded && Object.entries(substratums).reduce(
          (result, [propName, substratum]) => (
            result.concat(
              substratum?.map(($widget) => (
                <ElementItem key={$widget.uid} prefix={propName !== 'children' && propName} level={level + 1} widget={$widget} />
              ))
            )
          ),
          []
        )}
      </Collapse>
    </>
  );
}

export default function ElementStructure({ open, onActived, onAppend, onDestroy, onModify, onReadyEdit, onSort }) {
  const { getFixedT: dt } = useLocales();
  const { widgets, onListenersActived } = useWidgetContext();
  const { children: substratum } = useSubstratumWidgets({ stringify: false });

  const [dnd, dispatch, handleDnd] = useDnd(widgets, onSort);

  const appbarRef = useRef();
  const classes = useStyles();
  const [nodeValue, setExpandeds] = useNodeProviderValue(appbarRef, dnd, dispatch, onActived, onAppend, onDestroy, onModify);

  useEffect(() => {
    if (open) {
      onListenersActived(false);
    }
  }, [open]);

  return (
    <>
      <AppBar ref={appbarRef} position="static" color="inherit" className={classes.appbar} elevation={0}>
        <Toolbar variant="dense">
          <Typography variant="subtitle1" color="primary">
            {dt('ttl-elements')}
          </Typography>

          <Tooltip title={dt('btn-edit-ready-handle')}>
            <IconButton size="small" color="primary" onClick={onReadyEdit}>
              <QueuePlayNextIcon />
            </IconButton>
          </Tooltip>

          <Tooltip title={dt('btn-create-element')}>
            <IconButton size="small" color="primary" onClick={() => nodeValue.onAppend()}>
              <AddIcon />
            </IconButton>
          </Tooltip>

          <Divider flexItem orientation="vertical" />

          <Switch
            color="secondary"
            size="small"
            checked={dnd.enabled}
            onChange={({ target: { checked } }) => {
              dispatch({ type: 'switch', value: checked });
              setExpandeds(new Set());
            }}
          />
        </Toolbar>
      </AppBar>

      <DragDropContext onDragStart={handleDnd.start} onDragUpdate={handleDnd.update} onDragEnd={handleDnd.end}>
        <Droppable isCombineEnabled direction="vertical" droppableId={ROOT_ID} isDropDisabled={!dnd.enabled}>
          {(dropProv) => (
            <NodeContext.Provider value={nodeValue}>
              <List {...dropProv.droppableProps} ref={dropProv.innerRef} role="element" className={classes.root}>
                {substratum?.map((widget) => (
                  <ElementItem key={widget.uid} widget={widget} />
                ))}

                {dropProv.placeholder}
              </List>
            </NodeContext.Provider>
          )}
        </Droppable>
      </DragDropContext>
    </>
  );
}
