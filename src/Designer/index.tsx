import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { SnackbarProvider } from 'notistack';

import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Container from '@material-ui/core/Container';
import Drawer from '@material-ui/core/Drawer';
import InputAdornment from '@material-ui/core/InputAdornment';
import MuiPickersUtilsProvider from '@material-ui/pickers/MuiPickersUtilsProvider';
import TextField from '@material-ui/core/TextField';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';

import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';

import DateFnsUtils from '@date-io/date-fns';
import _delay from 'lodash/delay';
import cx from 'clsx';
import { AppcraftParser } from '@appcraft/parser';
import { generate as uuid } from 'shortid';

import ElementStructure from './element-structure';
import PropsEditor from './props-editor';
import TodoEditor from './todo-editor';
import makeLocales, { useLocales } from '../_utils/locales';
import { ProptypesEditorContext, useControlValue } from './_customs';
import { WidgetWrapper, WidgetImplement, DefinitionsType, GlobalStateType, ReadyTodoType, StateBindingType, WidgetOptionsType } from '../Visualizer';

import LocalesEn from '../_assets/locales/en/designer.json';
import LocalesZh from '../_assets/locales/zh/designer.json';


//* Definitions
const PROP_TYPES = {
  actions: PropTypes.element,
  definitions: PropTypes.object.isRequired,
  value: PropTypes.object,
  widgetProxy: PropTypes.objectOf(PropTypes.elementType),

  onCancel: PropTypes.func,
  onConfirm: PropTypes.func,
  onJsonModeOpen: PropTypes.func,
  overrideMixedOptions: PropTypes.func,
  overridePropControl: PropTypes.func,

  InputStyles: PropTypes.exact({
    color: PropTypes.oneOf<'primary' | 'secondary'>(['primary', 'secondary']),
    margin: PropTypes.oneOf<'dense' | 'none' | 'normal'>(['dense', 'none', 'normal']),
    size: PropTypes.oneOf<'medium' | 'small'>(['medium', 'small']),
    variant: PropTypes.oneOf<'filled' | 'outlined' | 'standard'>(['filled', 'outlined', 'standard'])
  }),
  classes: PropTypes.exact({
    root: PropTypes.string,
    header: PropTypes.string,
    drawer: PropTypes.string,
    footer: PropTypes.string,
    required: PropTypes.string,
    structure: PropTypes.string
  })
};

namespace AppcraftDesigner {
  export namespace def {
    type DesignerValue = {
      subject?: string;
      ready?: ReadyTodoType;
      state?: GlobalStateType;
      widgets?: WidgetOptionsType[];
    };

    interface OverrideControlEvent<T extends AppcraftParser.def.PropType> extends Omit<AppcraftParser.def.PropDefinition<T>, 'options' | 'uid'> {
      pathname: string;
      propName: string;
    };

    interface OverrideMixedOptionEvent<T extends AppcraftParser.def.PropType> {
      pathname: string;
      options: AppcraftParser.def.PropDefinition<T>[];
    }

    export interface Props extends Pick<PropTypes.InferProps<typeof PROP_TYPES>, 'InputStyles' | 'actions' | 'classes' | 'widgetProxy'> {
      definitions: DefinitionsType;
      value?: DesignerValue;

      onCancel?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
      onConfirm?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, newValue: DesignerValue) => void;
      overrideMixedOptions?: <T extends AppcraftParser.def.PropType>(e: OverrideMixedOptionEvent<T>) => AppcraftParser.def.PropDefinition<T>[];
      overridePropControl?: <T extends AppcraftParser.def.PropType>(e: OverrideControlEvent<T>) => React.ElementType | null;

      onJsonModeOpen?: (
        e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
        props: WidgetOptionsType['props'],
        confirm: (props: WidgetOptionsType['props']) => void
      ) => void;
    };
  }

  export namespace hooks {
    type ControlParam = { type: symbol; target?: string; value?: any; options?: any; };

    type ControlState = {
      actived?: string;
      listeners: string[];
      subject: string;
      ready: ReadyTodoType;
      state: GlobalStateType;
      widgets: WidgetOptionsType[];
    };

    export type ControlValue = [Record<string, symbol>, ControlState, (e: ControlParam | ControlParam[]) => void];

    export interface EditorParam extends WidgetOptionsType {
      superior?: string;
      description: string;
    }

    export type ElementParam = { type: 'SET_STATE' | 'WIDGET_APPEND' | 'WIDGET_DESTROY'; target?: string; value?: any; options?: any; };

    export interface BindingParam extends StateBindingType {
      uid: string;
    }
  }
}

//* Custom Hooks
const ROOT_ID = uuid();

// @ts-ignore
const useReadyFnProviderValues = (InputStyles, classes, state, handles, onActive, onChange) => (
  useMemo(() => ({
    InputStyles,

    classes,
    uid: ROOT_ID,
    handles: { onReady: handles },
    state,

    onActive,
    onChange
  }), [state, handles])
);

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    overflow: 'hidden'
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    height: '100%',
    overflow: 'hidden',
    position: 'relative',
    paddingRight: theme.spacing(42),

    '& > .AppcraftDesigner-content': {
      display: 'flex',
      flexDirection: 'column',
      height: '100%'
    }
  },
  drawer: {
    position: 'absolute',
    borderLeft: `1px solid ${theme.palette.divider}`,
    overflow: 'hidden !important',
    width: 0,
    zIndex: theme.zIndex.drawer,

    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),

    '&.open': {
      width: theme.spacing(42),

      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    }
  },
  appbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,

    '&.AppcraftDesigner-footerbar': {
      marginTop: 'auto',
      bottom: 0,

      '& > * > button': {
        borderTopLeftRadius: '0 !important',
        borderTopRightRadius: '0 !important'
      }
    }
  },
  structure: {
    height: '100%'
  },
  title: {
    '&:hover .AppcraftDesigner-tagIcon': {
      color: theme.palette.primary.main
    }
  }
}));


//* Component
const Designer: React.FC<AppcraftDesigner.def.Props> = ({
  InputStyles,
  actions,
  classes: $classes,
  definitions,
  value: defaultValue,
  widgetProxy,
  overrideMixedOptions,
  overridePropControl,
  onCancel,
  onConfirm,
  onJsonModeOpen
}) => {
  const { getFixedT: dt } = useLocales();
  const [CONTROL_ACTION, { actived, subject, ready, state: globalState, widgets }, dispatch] = useControlValue(defaultValue || {}) as AppcraftDesigner.hooks.ControlValue;

  const classes = useStyles();
  const refresh = useMemo(() => uuid(), [globalState]);

  const state = useMemo(() => (
    Object.entries(globalState).map(([widgetUid, list]) => (
      list.map((widgetState) => ({
        ...widgetState,
        widgetUid,
        widgetDesc: widgets.find(({ uid }) => uid === widgetUid).description
      }))
    )).flat()
  ), [globalState, widgets]);

  const styles = useMemo(() => ({
    color: InputStyles?.color || 'primary',
    margin: InputStyles?.margin,
    size: InputStyles?.size || 'small',
    variant: InputStyles?.variant || 'outlined'
  }), [InputStyles]);

  const readyProviderValues = useReadyFnProviderValues(
    styles,
    { structure: cx(classes.structure, $classes?.structure), drawerPaper: cx(classes.drawer, $classes?.drawer) },
    state,
    ready,
    () => dispatch({ type: CONTROL_ACTION.SET_STATE, target: 'actived', value: null }), // @ts-ignore
    ({ handles }) => dispatch({ type: CONTROL_ACTION.RESET_READY, value: handles?.onReady || [] })
  );

  return (
    <SnackbarProvider maxSnack={4} autoHideDuration={50000}>
      <WidgetWrapper key={refresh} definitions={definitions} proxy={widgetProxy} state={globalState} widgets={widgets}>
        <Container disableGutters maxWidth={false} className={cx(classes.root, $classes?.root)}>
          <Container disableGutters maxWidth={false} className={classes.container}>
            <Container disableGutters maxWidth={false} className="AppcraftDesigner-content">
              {/* TODO: Header Toolbar */}
              <AppBar position="static" color="inherit" className={cx(classes.appbar, $classes?.header)} elevation={0}>
                <Toolbar variant="dense">
                  <TextField
                    fullWidth
                    className={classes.title}
                    variant="standard"
                    size="small"
                    value={subject}
                    onChange={({ target: { value } }) => dispatch({ type: CONTROL_ACTION.SET_STATE, target: 'subject', value })}
                    InputProps={{
                      disableUnderline: true,
                      startAdornment: (
                        <InputAdornment position="start">
                          <Tooltip title={dt('lbl-description')}>
                            <LabelImportantIcon className="AppcraftDesigner-tagIcon" />
                          </Tooltip>
                        </InputAdornment>
                      )
                    }}
                  />

                  {actions}
                </Toolbar>
              </AppBar>

              <WidgetImplement ready={ready} lazyDeps={[Object.keys(globalState), widgets, ready]} />
            </Container>

            {/* TODO: Props Setting */}
            <Drawer
              anchor="right"
              variant="permanent"
              classes={{ paper: cx(classes.drawer, $classes?.drawer, { open: Boolean(actived) }) }}
              open={Boolean(actived)}
            >
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                {actived && (
                  actived === ROOT_ID
                    ? ( // @ts-ignore
                      <ProptypesEditorContext.Provider value={readyProviderValues}>
                        <TodoEditor refs={{ state: globalState, input: [], todo: {} }} definition={{ type: 'func' }} propName="onReady" />
                      </ProptypesEditorContext.Provider>
                    )
                    : (
                      <PropsEditor
                        InputStyles={styles}
                        state={state}
                        value={actived}
                        overrideMixedOptions={overrideMixedOptions}
                        overridePropControl={overridePropControl}

                        classes={{
                          structure: cx(classes.structure, $classes?.structure),
                          drawerPaper: cx(classes.drawer, $classes?.drawer)
                        }}
                        onJsonModeOpen={onJsonModeOpen}
                        onChange={(e: AppcraftDesigner.hooks.EditorParam) => (
                          dispatch({ type: CONTROL_ACTION.WIDGET_MODIFY, value: e })
                        )}
                        onElementDispatch={(e: AppcraftDesigner.hooks.ElementParam | AppcraftDesigner.hooks.ElementParam[]) => (
                          dispatch((Array.isArray(e) ? e : [e]).map(({ type, ...param }) => ({ type: CONTROL_ACTION[type], ...param })))
                        )}
                        onStateBinding={({ uid: target, path: value, ...options }: AppcraftDesigner.hooks.BindingParam, checked: boolean) => (
                          dispatch({ type: CONTROL_ACTION[checked ? 'STATE_APPEND' : 'STATE_DESTROY'], target, value, options })
                        )}
                      />
                    )
                )}
              </MuiPickersUtilsProvider>
            </Drawer>

            {/* TODO: Element Structure */}
            <Drawer
              anchor="right"
              variant="permanent"
              classes={{ paper: cx(classes.drawer, $classes?.drawer, { open: Boolean(!actived) }) }}
              open={Boolean(!actived)}
            >
              <ElementStructure
                open={Boolean(!actived)}
                onActived={(value: string) => dispatch({ type: CONTROL_ACTION.SET_STATE, target: 'actived', value })}
                onAppend={(e: AppcraftDesigner.hooks.EditorParam) => dispatch({ ...e, type: CONTROL_ACTION.WIDGET_APPEND })}
                onDestroy={(target: string) => dispatch({ type: CONTROL_ACTION.WIDGET_DESTROY, target })}
                onModify={(e: AppcraftDesigner.hooks.EditorParam) => dispatch({ type: CONTROL_ACTION.WIDGET_MODIFY, value: e })}
                onReadyEdit={() => dispatch({ type: CONTROL_ACTION.SET_STATE, target: 'actived', value: ROOT_ID })}
                onSort={(e: WidgetOptionsType[]) => dispatch({ type: CONTROL_ACTION.WIDGET_SORT, value: e })}
              />
            </Drawer>
          </Container>

          {/* TODO: Footer Toolbar */}
          {(onCancel instanceof Function || onConfirm instanceof Function) && (
            <AppBar component="footer" position="sticky" color="inherit" className={cx(classes.appbar, $classes?.footer, 'AppcraftDesigner-footerbar')} elevation={0}>
              {/* @ts-ignore */}
              <Toolbar disableGutters fullWidth variant="dense" size="large" component={ButtonGroup}>
                {onCancel instanceof Function && (
                  <Button
                    variant="contained"
                    color="default"
                    startIcon={(<CloseIcon />)}
                    onClick={onCancel}
                  >
                    {dt('btn-cancel')}
                  </Button>
                )}

                {onConfirm instanceof Function && (
                  <Button
                    variant="contained"
                    color="primary"
                    startIcon={(<CheckIcon />)}
                    onClick={(e) => onConfirm(e, { subject, ready, state: globalState, widgets })}
                  >
                    {dt('btn-confirm')}
                  </Button>
                )}
              </Toolbar>
            </AppBar>
          )}
        </Container>
      </WidgetWrapper>
    </SnackbarProvider>
  );
};

Designer.displayName = 'AppcraftDesigner';
Designer.propTypes = PROP_TYPES;

export default makeLocales({
  en: LocalesEn,
  zh: LocalesZh
})(Designer);
