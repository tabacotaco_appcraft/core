# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.4](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.1.3...v1.1.4) (2022-09-06)


### Bug Fixes

* package the provider values with useMemo ([b2ec2af](https://gitlab.com/tabacotaco_appcraft/core/commit/b2ec2af6982aad8c9870a5749a9d21f7d52d33a0))

### [1.1.3](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.1.2...v1.1.3) (2022-09-06)


### Bug Fixes

* **designer:** fixed dnd bugs ([4785d76](https://gitlab.com/tabacotaco_appcraft/core/commit/4785d762c6e2629916c9a18150ccc997ea91b28e))

### [1.1.2](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.1.1...v1.1.2) (2022-08-16)

### [1.1.1](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.1.0...v1.1.1) (2022-07-27)


### Bug Fixes

* **decorations:** 修正 import 路徑 ([fe08aae](https://gitlab.com/tabacotaco_appcraft/core/commit/fe08aae2e6eb4dd725f2946f86b13f369fdaf9c6))

## [1.1.0](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.0.5...v1.1.0) (2022-07-27)


### Features

* **with-formdata:** add new decoration(with-formdata) for binding data object quickly ([9beb186](https://gitlab.com/tabacotaco_appcraft/core/commit/9beb1860375d2aa3510f45d90dc437d7d33bd0cf))

### [1.0.5](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.0.4...v1.0.5) (2022-07-25)


### Bug Fixes

* **designer:** 修正拖曳重新排序時, 不可放置到自己結構下 ([23ee13f](https://gitlab.com/tabacotaco_appcraft/core/commit/23ee13fd7f2c26c9ae3ff633b314e945e6e92320))

### [1.0.4](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.0.3...v1.0.4) (2022-07-21)


### Bug Fixes

* **visualizer:** 修正初始化時, global state 可能會被覆蓋為預設值的錯誤 ([072c8ab](https://gitlab.com/tabacotaco_appcraft/core/commit/072c8ab7dfbb9f246d6f7e4d1a4f029066294223))

### [1.0.3](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.0.2...v1.0.3) (2022-07-21)

### [1.0.2](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.0.1...v1.0.2) (2022-07-21)


### Bug Fixes

* **definitions:** 修正 definitions 內提供的 component import path ([f13fc28](https://gitlab.com/tabacotaco_appcraft/core/commit/f13fc283b384ec0590588768563d07c502270428))
* **definitions:** 修正 definitions 提供 component/hoc import path ([0b4f4f9](https://gitlab.com/tabacotaco_appcraft/core/commit/0b4f4f9a2e67e9fcb1fc41e9617c1e4e97ea47d1))

### [1.0.2](https://gitlab.com/tabacotaco_appcraft/core/compare/v1.0.1...v1.0.2) (2022-07-21)


### Bug Fixes

* **definitions:** 修正 definitions 提供 component/hoc import path ([0b4f4f9](https://gitlab.com/tabacotaco_appcraft/core/commit/0b4f4f9a2e67e9fcb1fc41e9617c1e4e97ea47d1))

### 1.0.1 (2022-07-20)

### 1.0.1 (2022-07-20)

### [1.14.3](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/compare/v1.14.2...v1.14.3) (2022-07-13)


### Bug Fixes

* **designer:** fixed the element sorting bugs ([d11966b](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/commit/d11966b89fd38eeeb2a0c8fb26fe917f29915f2b))

### [1.14.2](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/compare/v1.14.1...v1.14.2) (2022-07-13)


### Bug Fixes

* **designer:** fixed dnd sorting bugs ([eb92171](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/commit/eb9217150adb58e2da24024ed2d5187cd6f30f94))

### [1.14.1](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/compare/v1.14.0...v1.14.1) (2022-07-13)


### Bug Fixes

* **designer:** fixed the dnd sorting bugs ([db92e12](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/commit/db92e124ebf50396435acf21fcbd02a513b9efa9))

## [1.14.0](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/compare/v1.13.15...v1.14.0) (2022-07-13)


### Features

* **designer:** add dnd sorting feature ([e162b71](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/commit/e162b7188e96fd9daef14616960c9436b7958edb))
* **widget:** add index to widget options ([c151e6e](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/commit/c151e6e7c564ef2943c21295bf3d6c57fd1f74a7))

### [1.13.15](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/compare/v1.13.14...v1.13.15) (2022-06-29)


### Others

* **.versionrc:** update info ([0dc95ea](https://git.gorilla-technology.com/gorilla/appcraft/visualizer/commit/0dc95ea48100ac9bbdbe7437adbb5d3f91a06e41))

### [1.13.14](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.13...v1.13.14) (2022-06-28)


### Bug Fixes

* **decorations-editor:** fixed bugs ([b522db2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/b522db2a88c8dabacdd9118d4caca21494d6f042))

### [1.13.13](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.12...v1.13.13) (2022-06-28)


### Bug Fixes

* **designer:** 修正事件編輯參考值選項產生規則 ([367f6e3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/367f6e3dae75f0689947e80f8eaa0a13e5c91259))

### [1.13.12](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.11...v1.13.12) (2022-06-22)


### Bug Fixes

* **designer:** 修正刪除widget時,會連帶移除所有事件todo內的set state 設定 ([364f20a](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/364f20afdd4c1069a9735df466fbc2d598b7ebcc))

### [1.13.11](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.10...v1.13.11) (2022-06-14)


### Bug Fixes

* **locales:** update zh-Hant as zh ([0aa6679](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/0aa6679a1ca3e843e5f42fdf218740c95b99cfe3))

### [1.13.10](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.9...v1.13.10) (2022-06-13)


### Bug Fixes

* event input params fixed ([6d7e755](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/6d7e755de9423285dc51e1240fa4fd8de560fb66))

### [1.13.9](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.8...v1.13.9) (2022-05-27)


### Bug Fixes

* **get-prop-pathname:** fixed bugs ([ceeed07](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/ceeed0709011e10fb8fb9c5b90b3a2035e19b8a7))

### [1.13.8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.7...v1.13.8) (2022-05-27)


### Bug Fixes

* **getproppathname:** fixed bugs ([3d0604f](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/3d0604ff1333d673fcda469bdeab308d307d443d))

### [1.13.7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.6...v1.13.7) (2022-05-04)


### Bug Fixes

* fixed bugs ([fad9ee1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/fad9ee1ef3ccef11d24a3e3935c86e925a25ab9c))

### [1.13.6](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.5...v1.13.6) (2022-05-04)


### Bug Fixes

* remove console ([77e9de3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/77e9de31c6d3b9411d3151a537e2d3242645fd77))

### [1.13.5](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.4...v1.13.5) (2022-05-04)


### Bug Fixes

* **template process:** add todo type to json stringify ([bf2b754](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/bf2b754890bafa966366810e97a878674b670740))

### [1.13.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.3...v1.13.4) (2022-05-04)


### Bug Fixes

* **todo request:** that will do nothing when request error ([432c377](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/432c377e5a16538e551d4d7101ce8b39afa4372a))


### Others

* **examples\:** update examples ([7371d95](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/7371d95d08cc3248f6c21b89c726e99153dffa2d))

### [1.13.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.2...v1.13.3) (2022-04-20)


### Bug Fixes

* css fixed ([ce2bf38](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/ce2bf38010b612edd80607c6c544b7a538909642))

### [1.13.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.1...v1.13.2) (2022-04-18)


### Bug Fixes

* add json-editor feature ([0ba2b5f](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/0ba2b5fb7e140847ea3458b5f7d721050fb17e27))


### Others

* remove console ([95f612d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/95f612d1a2d4f0917640fd386d95238491bf2d67))

### [1.13.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.13.0...v1.13.1) (2022-04-15)


### Bug Fixes

* **designer:** fixed pure number display bug and editor header bar css ([8a15b05](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/8a15b0562cf7410fe8bff8e8e71f53c6a436581a))

## [1.13.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.12.4...v1.13.0) (2022-04-14)


### Features

* **designer:** add onJsonModeOpen event to support JSON Editor ([a27a298](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/a27a298a218f627f3d31e82a7ef6c48aae21992f))

### [1.12.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.12.3...v1.12.4) (2022-04-14)


### Bug Fixes

* **pure:** 修正 number 需要使用 defaultValue 串接 ([68cc59f](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/68cc59ff089bcad72abfba1d72ac126ffb451b58))

### [1.12.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.12.2...v1.12.3) (2022-04-14)


### Bug Fixes

* bugfixed ([5cd4ab1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/5cd4ab175e260128cc84b1bdd61bc25871b5b817))


### Others

* **examples:** update @appcraft/decorations ([5f5edab](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/5f5edab74401d255e7673c724b759c95badb5051))

### [1.12.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.12.1...v1.12.2) (2022-04-14)


### Bug Fixes

* **designer:** 提供更多參數給 overrided 使用 ([29cb392](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/29cb3925e34e8ab3852d880efcf89c1dd2e8d36a))

### [1.12.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.12.0...v1.12.1) (2022-04-14)


### Bug Fixes

* **decorations:** bugfixed ([99e9f9b](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/99e9f9b699e0a94b203667ebdb2420993e20c79b))


### Others

* **examples:** update ([44896be](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/44896bece0d8e684133790d96a3aa0f99421206c))

## [1.12.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.11.6...v1.12.0) (2022-04-13)


### Features

* **designer:** 修正 overrided field 的參數 ([321e3bd](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/321e3bd99095d16a2c6618754b4b490a0a79ab41))


### Others

* **examples:** update @appcraft/visualizer ([6dbad14](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/6dbad14ef8bf5d5e2befbc599e150f01c6ba7dce))

### [1.11.6](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.11.5...v1.11.6) (2022-04-13)


### Bug Fixes

* **map:** 修正 condition 設置 ([c50e09c](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/c50e09cb05b00cb643e0ec83b3a281699fffb660))

### [1.11.5](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.11.4...v1.11.5) (2022-04-12)


### Bug Fixes

* **todo:** remove map's pair condition property ([a972c8e](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/a972c8e593d2ac3d0f70669a60e2237e0cf26196))

### [1.11.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.11.3...v1.11.4) (2022-04-12)


### Bug Fixes

* fixed bugs ([6cfa924](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/6cfa9245befb641a8c79d75ede1f83a532c6199c))
* **todo:** 整合 property subheader component ([3e324ba](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/3e324baa0cfb3df4756114984c5f1c5958a56abe))
* **todo:** todo item element bugfixed ([03a8d8a](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/03a8d8ab9c643b0792b918d7054710bb75f0e3b3))

### [1.11.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.11.2...v1.11.3) (2022-04-11)


### Bug Fixes

* fixed useReducer bugs ([cee103e](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/cee103e305ec7abe2e336b4d3305a40406effd01))

### [1.11.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.11.1...v1.11.2) (2022-04-08)


### Bug Fixes

* **todo:** bugfixed ([49f4565](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/49f4565b69d3e73f3058c423529775ec731a37ae))


### Others

* **examples:** update version ([24b026d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/24b026d068b1954096d74a3e2c9e124122381be8))

### [1.11.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.11.0...v1.11.1) (2022-04-06)


### Bug Fixes

* **todo:** 修正 todo render 機制 ([7f2e0ff](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/7f2e0ff88a8753b6f2c4e49d1dc78947a50a068e))

## [1.11.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.10.8...v1.11.0) (2022-04-01)


### Features

* 調整事件設定方式 ([d1b142c](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/d1b142ccf02015c58d371351d3a07e7b67d9eda6))

### [1.10.8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.10.7...v1.10.8) (2022-03-31)


### Bug Fixes

* url bug fixeds ([ed9d02d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/ed9d02dcfdea7cc1c16d8fe2c0af84f0d1ed8f92))

### [1.10.7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.10.6...v1.10.7) (2022-03-30)


### Bug Fixes

* 效能修正 ([f4676a7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/f4676a71b77ecaa23e5336eb5dce72157d5eecac))

### [1.10.6](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.10.5...v1.10.6) (2022-03-30)


### Bug Fixes

* bugfixed ([87d0069](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/87d00694126348a9ecefe1ede53bccb7ef4f22dd))

### [1.10.5](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.10.4...v1.10.5) (2022-03-30)


### Others

* add info ([b35d397](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/b35d397a5053885d8f12dda488bddaa5ebd29ade))
* **examples:** update packages ([b6fcb88](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/b6fcb888e8dc0276537ec60794e8d093a1c55eab))

### [1.10.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.10.3...v1.10.4) (2022-03-30)


### Bug Fixes

* bugfixed ([f092f50](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/f092f5063d05e0e4945103bb7174b0f1322ac098))


### Others

* update examples ([92621c1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/92621c168c0949d63257a43957b69deb0988003a))

### [1.10.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.10.2...v1.10.3) (2022-03-29)


### Bug Fixes

* **widget:** fixed bugs ([6a5f4f4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/6a5f4f4087ed2628d17c723c2c3b57fc6c39a39f))


### Others

* update examples packages ([c14fcb9](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/c14fcb974786a60b3d8ed61d9850bf2762aab259))

### [1.10.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.10.1...v1.10.2) (2022-03-29)


### Bug Fixes

* bugfixed ([858cd23](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/858cd234a4f5108d1a4ddbf81d6d1216e992c7b2))

### [1.10.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.10.0...v1.10.1) (2022-03-29)


### Bug Fixes

* bugfixed ([7eded86](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/7eded8632afc3da80e33aea282e7a0066fb57b8d))

## [1.10.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.9...v1.10.0) (2022-03-29)


### Features

* **widget:** 改變 render widget 的方式 ([e5a3908](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/e5a39087452c40b395a49fe7c49e73d45880e58a))

### [1.9.9](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.8...v1.9.9) (2022-03-28)


### Bug Fixes

* bugfixed ([4c05e06](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/4c05e06aa7e31e97c8db2c67d833aa372f84853d))

### [1.9.8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.7...v1.9.8) (2022-03-28)


### Bug Fixes

* bugfixed ([3c05537](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/3c055370b880c87548e95ae196db162f6b6db2b3))

### [1.9.7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.6...v1.9.7) (2022-03-28)


### Bug Fixes

* bugfixed ([18f081c](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/18f081c9b116a3109eab0f6e95acdbf917ef77b1))

### [1.9.6](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.5...v1.9.6) (2022-03-28)


### Bug Fixes

* bugfixed ([bab07e4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/bab07e4388f2795ee5504cd549646e9a8977c4eb))

### [1.9.5](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.4...v1.9.5) (2022-03-28)


### Bug Fixes

* **with-element-map:** bugfixed ([d4b89d3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/d4b89d36ab1245897a622f9cf0cac8058e5eed7a))

### [1.9.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.3...v1.9.4) (2022-03-28)


### Bug Fixes

* **with-element-map:** bugfixed ([54bd7d8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/54bd7d8db288265c4ba666c5da733a4dfd960dd6))

### [1.9.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.2...v1.9.3) (2022-03-25)


### Others

* rebuild ([97bc837](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/97bc8373c898e6c04e2721eb7738ae6522a6c7ca))

### [1.9.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.1...v1.9.2) (2022-03-25)


### Bug Fixes

* bugfixed ([811ffd2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/811ffd2db05a245fd2d7257055114957f69af2ea))

### [1.9.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.9.0...v1.9.1) (2022-03-25)


### Bug Fixes

* bugfixed ([ba6fe99](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/ba6fe9991c0c708c673d6b0b9f7134cb144c0c22))

## [1.9.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.8.1...v1.9.0) (2022-03-25)


### Features

* add decoration base ([c7bb6a7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/c7bb6a71d011cb2103013a3119735dc6346fc98e))


### Others

* **examples:** update packages ([570a3a1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/570a3a1c38342c354ab13b68fa0bc557e6faf3bd))

### [1.8.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.8.0...v1.8.1) (2022-03-25)


### Bug Fixes

* **variable:** fixed date process bugs ([cf6032f](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/cf6032fb109a8a4151d9fc16edb9473b5faf8746))

## [1.8.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.7.4...v1.8.0) (2022-03-25)


### Features

* add date pure ([c30171f](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/c30171f016988282a20b07522d68962d0039238a))


### Others

* **examples:** update packages ([7965321](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/7965321319e965bd5bada6b5b0e24a4b90bf9303))

### [1.7.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.7.3...v1.7.4) (2022-03-25)


### Bug Fixes

* **definitions:** re-generate ([d33835e](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/d33835e1e287622ad7c746e4005840bbae004f36))

### [1.7.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.7.2...v1.7.3) (2022-03-25)


### Others

* **package:** update @appcraft/prop-types-parser ([fead5b7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/fead5b71d8ed6b40f065c1703d4c845b58d98ec8))

### [1.7.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.7.1...v1.7.2) (2022-03-25)


### Others

* rebuild ([494b76c](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/494b76c365cbcb88fb85df48f9fcc43357b935fa))

### [1.7.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.7.0...v1.7.1) (2022-03-25)


### Bug Fixes

* **pickers:** fixed pickers value propTypes ([e47719e](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/e47719ee9aa13645c258e29433ccd18ab1d35264))

## [1.7.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.6.2...v1.7.0) (2022-03-25)


### Features

* **definition:** add material-ui pickers ([d07cbb2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/d07cbb2c5c1fb6cf05f763fcd1fae841d60a10b5))

### [1.6.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.6.1...v1.6.2) (2022-03-25)

### [1.6.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.6.0...v1.6.1) (2022-03-25)


### Others

* **gitignore:** add dist folder ([f8dee1a](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/f8dee1a8c801e14e7d8d5e8d9e3505d93f8cf439))

## [1.6.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.5.4...v1.6.0) (2022-03-25)


### Features

* add auto generate @material-ui/core definition json and export ([18fe5cb](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/18fe5cbca62676d55dd0f8d3d7bdab39f4bd8239))


### Others

* **examples:** update packages ([06e4d22](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/06e4d2225c09e853b1ba3cc6504341b1d91258e7))

### [1.5.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.5.3...v1.5.4) (2022-03-24)


### Bug Fixes

* **designer:** bugfixed ([286aad7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/286aad792050e0b8c7d31dc72515311ecb085861))

### [1.5.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.5.2...v1.5.3) (2022-03-24)


### Bug Fixes

* **pure-item:** fixed boolean switch can't get corret value ([b0123a4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/b0123a474f6bb443c35243832caafd8659bf509b))

### [1.5.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.5.1...v1.5.2) (2022-03-24)


### Bug Fixes

* **with-propitem:** fixed remove property bug ([5f0f506](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/5f0f5060f2c9e359d135bf49547056b8cbd84fc3))

### [1.5.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.5.0...v1.5.1) (2022-03-24)


### Bug Fixes

* **designer:** 修正 decoration 的 overrided props disabled 錯誤 ([63f5f40](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/63f5f40e26e597e02b543ad596c50741c67fd0ad))

## [1.5.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.4.5...v1.5.0) (2022-03-23)


### Features

* **todo:** add map todo ([b8fce58](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/b8fce58fbfac268c0a07e7da1832ce5b9be692ba))

### [1.4.5](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.4.4...v1.4.5) (2022-03-21)


### Bug Fixes

* bug fixed ([2be522d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/2be522d82fce3d28a660069422cd24c2b4a8c7d7))

### [1.4.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.4.3...v1.4.4) (2022-03-21)


### Others

* update ([ea5cdf0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/ea5cdf0340abc537db501bacea89f5f1de8c7445))

### [1.4.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.4.2...v1.4.3) (2022-03-21)


### Bug Fixes

* **element-structure:** 修正判斷能不能增加 children 的邏輯 ([b7b9f31](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/b7b9f3168f530bfae489677edf13b7b19a76f173))

### [1.4.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.4.1...v1.4.2) (2022-03-21)


### Bug Fixes

* **designer:** decoration 中的 function prop 不需要觸發就可以編輯參數 ([6f29cef](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/6f29cefc7ea20d995e70b8f5561b250eb1ab78ed))


### Others

* **examples:** update packages ([4f435cb](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/4f435cbcda21c4aa76af3a413388fc619187b733))

### [1.4.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.4.0...v1.4.1) (2022-03-21)


### Others

* update @appcraft/prop-types-parser ([6db5b95](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/6db5b957fca7eb6910d62bf131570839c306c6b2))

## [1.4.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.11...v1.4.0) (2022-03-21)


### Features

* **designer:** add auto disabled prop-items, if it has been overrided by hoc ([5a86dc3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/5a86dc3f1f12fec53c071b2a7ef7ebc5b6e146ed))


### Others

* **examples:** update [@appcraft](https://git.gorilla-technology.com/appcraft) ([5bb4b05](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/5bb4b05679610cd2761397ce791d14582875f44b))

### [1.3.11](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.10...v1.3.11) (2022-03-19)


### Bug Fixes

* **designer:** fixed state checking error ([c825a8d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/c825a8dc4619c431756e6a0ae65846eb13dccdc4))


### Others

* **examples:** update packages ([78f9498](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/78f949899f3226a56a7949a79804aa7840d05957))

### [1.3.10](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.9...v1.3.10) (2022-03-18)


### Others

* **package:** update @appcraft/prop-types-parser ([0247a1a](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/0247a1aa174f801b4abf64954d0c648a627a6a39))

### [1.3.9](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.8...v1.3.9) (2022-03-18)


### Bug Fixes

* **designer:** fixed actived function error ([c0b58e2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/c0b58e28878a50ad5709e346d3cb2fc962536749))


### Others

* **examples:** update @appcraft/visualizer ([0542ff6](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/0542ff6f979571b8efba22ca266decb84a4d97a4))

### [1.3.8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.7...v1.3.8) (2022-03-18)


### Bug Fixes

* **designer:** fixed the actived checked method ([86fc128](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/86fc12851b738bb0413cda6641bdab7f9b5005f1))

### [1.3.7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.6...v1.3.7) (2022-03-18)


### Others

* **package:** update @appcraft/prop-types-parser ([d2e900d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/d2e900d541ce1f3320e4db710d6875c085698292))
* update exampels ([66ef90c](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/66ef90c00815de668fa4661e526ea6cfcfec9df4))

### [1.3.6](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.5...v1.3.6) (2022-03-16)


### Bug Fixes

* ts component add displayName ([a96b8b0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/a96b8b0165327994693e1da458c1911a13f5edf4))


### Others

* **examples:** update @appcraft/visualizer ([4546485](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/4546485d22dcc13886a9756efe2b72a74629b287))

### [1.3.5](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.4...v1.3.5) (2022-03-16)


### Others

* **package:** remove react-beautiful-dnd ([c80bec8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/c80bec8be5993ed07bdb14f0a9ad1d4b1cb76321))

### [1.3.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.3...v1.3.4) (2022-03-16)


### Others

* **package:** update dependencies ([6c066cf](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/6c066cf9d1315a255e21034d51d3ac3257cac2b5))

### [1.3.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.2...v1.3.3) (2022-03-16)


### Others

* **examples:** update @appcraft/visualizer ([0102da5](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/0102da5d872544d8352a7a63e5cab2bcf7dc7f80))
* **npmignore:** update ignore target ([a1c2834](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/a1c2834135a227dbcf1cecffbf9c480d18c65383))

### [1.3.2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.1...v1.3.2) (2022-03-16)


### Others

* **publish:** for publishing test ([328b3a8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/328b3a87c8ee7bc05dfb425655f245e7cc6302be))

### [1.3.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.3.0...v1.3.1) (2022-03-16)


### Bug Fixes

* **designer:** 修正 decoration dialog CSS ([e0f7dae](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/e0f7dae3aeb5d6c780ff8898c965c53ccbeef879))

## [1.3.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.2.0...v1.3.0) (2022-03-16)


### Features

* **designer:** add hoc setting feature ([7abc57b](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/7abc57bb5016ebce9212232faed63c2d3e8880f8))

## [1.2.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.1.1...v1.2.0) (2022-03-15)


### Features

* **designer:** prepare to append hoc selection ([1a18721](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/1a18721f12fcb27bb5fd92f59b9e7ff09f710e87))

### [1.1.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.1.0...v1.1.1) (2022-03-15)


### Bug Fixes

* **designer:** 修正 todo condition 執行上的應用錯誤問題 ([181f67f](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/181f67f6b46a3b76fba91ec2a4e844f8930f047e))

## [1.1.0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v1.0.1...v1.1.0) (2022-03-15)


### Features

* **designer:** add condition setting for Todo ([fbd9808](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/fbd980887cc4231c4706fdb50f14fb53fc3b361c))

### [1.0.1](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.32...v1.0.1) (2022-03-14)


### Others

* **package.json:** update version ([3208ab7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/3208ab7a9970e9b5d98c3adcfa52da3f4d3b81fd))

### [0.0.32](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.31...v0.0.32) (2022-03-14)


### Bug Fixes

* add react.memo ([3b43a97](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/3b43a971e280481bf1d73a8915b242afed746659))

### [0.0.31](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.30...v0.0.31) (2022-03-04)


### Features

* **designer:** could edit onReady handle now ([8e246d2](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/8e246d2ed8033e6ca6cbc1f233c3f678d3d59bae))

### [0.0.30](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.29...v0.0.30) (2022-03-02)


### Bug Fixes

* **property:** update the naming button color as primary ([592dabd](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/592dabd1d44c44518f75c5e6424341b6be86bcb7))

### [0.0.29](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.28...v0.0.29) (2022-03-02)


### Bug Fixes

* **mixed:** 修正 type 改變後，property 操作方式沒有連動的錯誤 ([4268cc0](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/4268cc092e56490e4ca2c95707357d7cac9ceebe))

### [0.0.28](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.27...v0.0.28) (2022-03-02)


### Bug Fixes

* **designer:** 修正移除 widget 相關 state 的設定都要忽略，避免 undefined 錯誤 ([e5902cb](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/e5902cbb90c841a149a256e5455ceb865cfc7748))

### [0.0.27](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.26...v0.0.27) (2022-02-25)


### Features

* add locales process ([708adf9](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/708adf921c1220c8235046ebb652aee32ae98257))

### [0.0.26](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.25...v0.0.26) (2022-02-25)


### Others

* **package.json:** update @appcraft/visualizer ([5eab712](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/5eab7128f28d5206ae152210280c75475dbcf060))

### [0.0.25](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.24...v0.0.25) (2022-02-24)


### Others

* **package.json:** update @appcraft/prop-types-parser ([bb61d1b](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/bb61d1b2c4212125a77210d8d859724a15b83fb0))

### [0.0.24](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.23...v0.0.24) (2022-02-23)


### Bug Fixes

* bugfixed ([4566344](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/45663442e99ddee19df4778d90ab2e671146d33b))

### [0.0.23](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.22...v0.0.23) (2022-02-23)


### Bug Fixes

* bugfixed ([721dce9](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/721dce9de69f9b33bda3c8b47a8421bb5c32d5c5))

### [0.0.22](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.21...v0.0.22) (2022-02-22)


### Features

* add variable editor ([30564da](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/30564da8b99110ace260fbc7cc35844bd83b5cea))

### [0.0.21](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.20...v0.0.21) (2022-02-21)


### Features

* **variable:** variable eidtor ([24bdc8d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/24bdc8dbf5b499d58d4a3395bbb2e713079ff4ef))

### [0.0.20](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.19...v0.0.20) (2022-02-21)


### Features

* update ([9fb4e62](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/9fb4e62989a93e0bce01193318a4b1d7cb0b77fc))

### [0.0.19](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.18...v0.0.19) (2022-02-21)


### Features

* update source code structure ([6f1bb4d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/6f1bb4dfbc1a6072c2efb6e4b77725dc11183a18))

### [0.0.18](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.17...v0.0.18) (2022-02-19)


### Others

* structure update ([e1f3072](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/e1f30729480a658f23ff4d5fba5b205a848ba748))

### [0.0.17](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.16...v0.0.17) (2022-02-18)


### Features

* add a lot new features ([64cd455](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/64cd4553cf6e70984e0a6f85fb69f836a7223389))

### [0.0.16](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.15...v0.0.16) (2022-02-17)


### Others

* rebuild ([6454f70](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/6454f703e8e1a0a37c2416951bb833b17190b06a))

### [0.0.15](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.14...v0.0.15) (2022-02-17)


### Features

* rename ([caba8dd](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/caba8dd4d2e8ce7dff432b1d3da6f4a4549c47a5))

### [0.0.14](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.13...v0.0.14) (2022-02-17)


### Features

* add a lot features ([d21e854](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/d21e854c758c077609141478fd24cd36e4ced99b))

### [0.0.13](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.12...v0.0.13) (2022-02-14)


### Features

* **designer:** 增加支援 node/element props 編輯功能 ([75a943d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/75a943dba4d3161bea93529f1a5d05b2d169f430))

### [0.0.12](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.11...v0.0.12) (2022-02-07)


### Others

* bugfixed ([15ce981](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/15ce981b5a3ab13b8db2a89deffe9ad21da60284))

### [0.0.11](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.10...v0.0.11) (2022-01-28)


### Features

* **prop-editor:** 快完成了 ([c137699](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/c137699fa7c2f784ead84b1b27fb3ca5bbfd4d46))
* **prop-editor:** 快要完成了啦!!!!!!!!!!! ([dbb2174](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/dbb2174888ffb19ddeb72781f193f8ee55794d4b))


### Others

* **examples:** update package ([03ca693](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/03ca693423a4c68787af520c5a88d493e296355b))

### [0.0.10](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.9...v0.0.10) (2022-01-27)


### Bug Fixes

* **props-editor:** fixed the description rule ([8a3600a](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/8a3600a8389ac580f8fa3f373215acbfce3a896c))

### [0.0.9](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.8...v0.0.9) (2022-01-27)


### Features

* **props-editor:** 確定 props-editor 的開發架構 ([090b6a4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/090b6a410482912ba600cbadb5cfb8b9698522dc))

### [0.0.8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.7...v0.0.8) (2022-01-26)


### Features

* update ([141d740](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/141d740c232d2e6aafa333ae49bdad69887c0867))

### [0.0.7](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.6...v0.0.7) (2022-01-17)


### Features

* init ([5b4a5b8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/5b4a5b8649954cc2e3841baf94c7aef13bec82fc))

### [0.0.6](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.5...v0.0.6) (2022-01-12)


### Others

* init ([be5f8bd](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/be5f8bd5568349512db2131f1670672e90d7144e))

### [0.0.5](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.4...v0.0.5) (2022-01-12)


### Others

* init ([9b35789](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/9b35789188cf21935bf28bc1069bb235a835be2f))

### [0.0.4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.3...v0.0.4) (2022-01-12)


### Others

* init ([901f5a4](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/901f5a434638fc6e48ca2164cc8196ecb84c8673))

### [0.0.3](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/compare/v0.0.2...v0.0.3) (2022-01-12)


### Others

* test ([d64609d](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/d64609d4bc1fd471b65e53a1342a297c918acf48))

### 0.0.2 (2021-12-29)


### Features

* init ([475271e](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/475271e2699cd0b36a4ed1d67747efe50a253df6))
* init ([fe54ad8](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/fe54ad828ed137f5be64c4df1bc58fab72a6e7fa))


### Others

* init ([41e3706](https://git.gorilla-technology.com/gorilla/f2e/gui-gis/commit/41e3706bb5fc1b7bc7489f47ecc490008f9276c0))
