# @appcraft/core

This is a plugin package for online web page design of React.

## Example/Demo
- [Example](https://gitlab.com/tabacotaco_appcraft/example)
- [Demo](https://tabacotaco_appcraft.gitlab.io/example/#/designer)