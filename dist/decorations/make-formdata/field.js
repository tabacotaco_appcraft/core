"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _get2 = _interopRequireDefault(require("lodash/get"));

var _merge2 = _interopRequireDefault(require("lodash/merge"));

var _omit2 = _interopRequireDefault(require("lodash/omit"));

var _set2 = _interopRequireDefault(require("lodash/set"));

var _toPath2 = _interopRequireDefault(require("lodash/toPath"));

var _container = require("./container");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var makeFormField = function makeFormField(_ref) {
  var valueSource = _ref.valueSource,
      targetProp = _ref.targetProp,
      handleChangeProp = _ref.handleChangeProp,
      handleValueExtraPath = _ref.handleValueExtraPath;
  var sourcePath = (0, _toPath2["default"])(valueSource);
  var targetPath = (0, _toPath2["default"])(targetProp);
  var handlePath = (0, _toPath2["default"])(handleChangeProp);
  return function (FieldElement) {
    var FormField = function FormField(implementProps) {
      var _useFormData = (0, _container.useFormData)(),
          data = _useFormData.data,
          onChange = _useFormData.onChange;

      var props = (0, _omit2["default"])(implementProps, [targetPath, handlePath]);
      var overrideds = (0, _react.useMemo)(function () {
        var result = {};
        (0, _set2["default"])(result, targetPath, (0, _get2["default"])(data, sourcePath) || null);
        (0, _set2["default"])(result, handleValueExtraPath, function () {
          var handleChange = (0, _get2["default"])(implementProps, handlePath);

          for (var _len = arguments.length, e = new Array(_len), _key = 0; _key < _len; _key++) {
            e[_key] = arguments[_key];
          }

          handleChange === null || handleChange === void 0 ? void 0 : handleChange.apply(void 0, e);
          onChange((0, _set2["default"])(_objectSpread({}, data), targetPath, (0, _get2["default"])(e, handleValueExtraPath) || null));
        });
        return result;
      }, [data, onChange, (0, _get2["default"])(implementProps, handlePath)]);
      return /*#__PURE__*/_react["default"].createElement(FieldElement, (0, _merge2["default"])(props, overrideds));
    };

    FormField.Naked = FieldElement;
    FormField.displayName = 'FormField';
    FormField.overrideds = [targetProp, handleChangeProp].filter(function (path) {
      return path === null || path === void 0 ? void 0 : path.trim();
    });
    return FormField;
  };
};

makeFormField.configTypes = {
  changePath: _propTypes["default"].string,
  handleValueExtraPath: _propTypes["default"].string,
  valueSource: _propTypes["default"].string.isRequired,
  targetProp: _propTypes["default"].string.isRequired
};
makeFormField.propTypes = {};
var _default = makeFormField;
exports["default"] = _default;