"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = ElementStructure;

var _react = _interopRequireWildcard(require("react"));

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _AppBar = _interopRequireDefault(require("@material-ui/core/AppBar"));

var _Collapse = _interopRequireDefault(require("@material-ui/core/Collapse"));

var _Divider = _interopRequireDefault(require("@material-ui/core/Divider"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var _List = _interopRequireDefault(require("@material-ui/core/List"));

var _ListItem = _interopRequireDefault(require("@material-ui/core/ListItem"));

var _ListItemIcon = _interopRequireDefault(require("@material-ui/core/ListItemIcon"));

var _ListItemSecondaryAction = _interopRequireDefault(require("@material-ui/core/ListItemSecondaryAction"));

var _ListItemText = _interopRequireDefault(require("@material-ui/core/ListItemText"));

var _Switch = _interopRequireDefault(require("@material-ui/core/Switch"));

var _Toolbar = _interopRequireDefault(require("@material-ui/core/Toolbar"));

var _Tooltip = _interopRequireDefault(require("@material-ui/core/Tooltip"));

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

var _styles = require("@material-ui/core/styles");

var _Add = _interopRequireDefault(require("@material-ui/icons/Add"));

var _ChevronRight = _interopRequireDefault(require("@material-ui/icons/ChevronRight"));

var _Close = _interopRequireDefault(require("@material-ui/icons/Close"));

var _ExpandMore = _interopRequireDefault(require("@material-ui/icons/ExpandMore"));

var _ImportExport = _interopRequireDefault(require("@material-ui/icons/ImportExport"));

var _QueuePlayNext = _interopRequireDefault(require("@material-ui/icons/QueuePlayNext"));

var _SettingsOutlined = _interopRequireDefault(require("@material-ui/icons/SettingsOutlined"));

var _get2 = _interopRequireDefault(require("lodash/get"));

var _set2 = _interopRequireDefault(require("lodash/set"));

var _toPath4 = _interopRequireDefault(require("lodash/toPath"));

var _clsx = _interopRequireDefault(require("clsx"));

var _iconMenuButton = _interopRequireWildcard(require("./icon-menu-button"));

var _customs = require("./_customs");

var _locales = require("../_utils/locales");

var _customs2 = require("../Visualizer/_customs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var ROOT_ID = '$root';

function isAppendable(importBy, definitions, substratums) {
  var _substratums$children;

  var childrenType = (0, _get2["default"])(definitions, ['props', importBy, 'propTypes', 'options', 'children', 'type']);
  var subCount = ((_substratums$children = substratums.children) === null || _substratums$children === void 0 ? void 0 : _substratums$children.length) || 0;
  return importBy && (childrenType === 'node' || childrenType === 'element' && subCount < 1);
}

function isCombineWithChild(widgets, draggableId, droppableId) {
  var _ref = widgets.find(function (_ref2) {
    var uid = _ref2.uid;
    return uid === droppableId;
  }) || {},
      _ref$superior = _ref.superior,
      superior = _ref$superior === void 0 ? null : _ref$superior;

  var _toPath2 = (0, _toPath4["default"])(superior),
      _toPath3 = _slicedToArray(_toPath2, 1),
      _toPath3$ = _toPath3[0],
      uid = _toPath3$ === void 0 ? null : _toPath3$;

  return uid === draggableId || (uid ? isCombineWithChild(widgets, draggableId, uid) : false);
} //* Custom Hooks


var NodeContext = /*#__PURE__*/(0, _react.createContext)({
  container: function container() {
    return [];
  },
  dnd: null,
  expandeds: new Set(),
  onActived: function onActived() {
    return null;
  },
  onAppend: function onAppend() {
    return null;
  },
  onDestroy: function onDestroy() {
    return null;
  },
  onDnd: function onDnd() {
    return null;
  },
  onElementChange: function onElementChange() {
    return null;
  },
  onModify: function onModify() {
    return null;
  }
});

var useDnd = function () {
  function reducer(state, action) {
    return (Array.isArray(action) ? action : [action]).reduce(function (prevState, _ref3) {
      var type = _ref3.type,
          value = _ref3.value;

      switch (type) {
        case 'done':
          return _objectSpread(_objectSpread({}, prevState), {}, {
            drag: null,
            drop: null
          });

        case 'switch':
          return _objectSpread(_objectSpread({}, prevState), {}, {
            enabled: value === true
          });

        case 'drag':
          {
            if (prevState.enabled) {
              return _objectSpread(_objectSpread({}, prevState), {}, {
                drag: value
              });
            }

            break;
          }

        case 'drop':
          {
            if (prevState.enabled) {
              return _objectSpread(_objectSpread({}, prevState), {}, {
                drop: value
              });
            }

            break;
          }

        default:
      }

      return prevState;
    }, state);
  }

  return function (widgets, onSort) {
    var _useWidgetContext = (0, _customs2.useWidgetContext)(),
        definitions = _useWidgetContext.definitions;

    var _useReducer = (0, _react.useReducer)(reducer, {
      drag: null,
      drop: null,
      enabled: false
    }),
        _useReducer2 = _slicedToArray(_useReducer, 2),
        dnd = _useReducer2[0],
        dispatch = _useReducer2[1];

    var drag = dnd.drag,
        drop = dnd.drop;
    return [dnd, dispatch, {
      start: (0, _react.useCallback)(function (_ref4) {
        var draggableId = _ref4.draggableId,
            source = _ref4.source;
        var superior = widgets[source.index].superior;
        dispatch([{
          type: 'drag',
          value: draggableId
        }, {
          type: 'drop',
          value: superior || ROOT_ID
        }]);
      }, [widgets]),
      update: (0, _react.useCallback)(function (_ref5) {
        var destination = _ref5.destination,
            combine = _ref5.combine;

        if (combine) {
          var _widgets$find = widgets.find(function (_ref6) {
            var uid = _ref6.uid;
            return uid === combine.draggableId;
          }),
              superior = _widgets$find.superior,
              importBy = _widgets$find.importBy;

          if (isAppendable(importBy, definitions, (0, _customs2.getSubstratumWidgets)(widgets, combine.draggableId, false))) {
            dispatch({
              type: 'drop',
              value: "".concat(combine.draggableId, ".children")
            });
          } else {
            dispatch({
              type: 'drop',
              value: superior || ROOT_ID
            });
          }
        } else if (destination) {
          var _superior = widgets[destination.index].superior;
          dispatch({
            type: 'drop',
            value: _superior || ROOT_ID
          });
        }
      }, [widgets, definitions]),
      end: (0, _react.useCallback)(function (_ref7) {
        var source = _ref7.source,
            combine = _ref7.combine,
            destination = _ref7.destination;

        if (!isCombineWithChild(widgets, drag, (0, _toPath4["default"])(drop)[0])) {
          if (combine && isAppendable(widgets.find(function (_ref8) {
            var uid = _ref8.uid;
            return uid === combine.draggableId;
          }).importBy, definitions, (0, _customs2.getSubstratumWidgets)(widgets, combine.draggableId, false))) {
            onSort(widgets.map(function (widget) {
              return _objectSpread(_objectSpread({}, widget), widget.uid === drag && {
                superior: drop
              });
            }));
          } else if (destination && destination.index !== source.index) {
            var _ref9 = [Math.min(source.index, destination.index), Math.max(source.index, destination.index) + 1],
                fm = _ref9[0],
                to = _ref9[1];
            var modifieds = widgets.slice(fm, to);
            var behind = source.index < destination.index;
            var target = modifieds[behind ? 'shift' : 'pop']();
            modifieds[behind ? 'push' : 'unshift'](target);
            onSort([].concat(_toConsumableArray(widgets.slice(0, fm)), _toConsumableArray(modifieds.map(function (widget, i) {
              return _objectSpread(_objectSpread({}, widget), {}, {
                index: fm + i
              }, widget.uid === drag && {
                superior: drop === ROOT_ID ? null : (0, _customs.getClosetElements)(widgets, widgets[destination.index].uid).includes(target.uid) ? target.superior : drop
              });
            })), _toConsumableArray(widgets.slice(to))));
          }
        }

        dispatch({
          type: 'done'
        });
      }, [drag, drop, widgets, definitions])
    }];
  };
}();

var useNodeProviderValue = function useNodeProviderValue(appbarRef, dnd, onDnd, onActived, onAppend, onDestroy, onModify) {
  var _useState = (0, _react.useState)(new Set()),
      _useState2 = _slicedToArray(_useState, 2),
      expandeds = _useState2[0],
      setExpandeds = _useState2[1];

  return [(0, _react.useMemo)(function () {
    return {
      container: function container() {
        return Array.from(appbarRef.current.nextSibling.querySelectorAll('li[data-widget]')).map(function (el) {
          return el.dataset.widget;
        });
      },
      dnd: dnd,
      expandeds: expandeds,
      onActived: onActived,
      onAppend: onAppend,
      onDestroy: onDestroy,
      onDnd: onDnd,
      onModify: onModify,
      onElementChange: function onElementChange(uid, isExpanded) {
        expandeds[isExpanded ? 'add' : 'delete'](uid);
        setExpandeds(new Set(expandeds));
      }
    };
  }, [expandeds, dnd]), setExpandeds];
};

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      display: 'flex',
      flexDirection: 'column',
      overflow: function overflow() {
        var _ref10 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
            level = _ref10.level;

        return "hidden ".concat(!level ? 'auto' : 'hidden', " !important");
      },
      width: '100%',
      height: function height() {
        var _ref11 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
            level = _ref11.level;

        return !level ? '100%' : 'fit-content';
      }
    },
    appbar: {
      borderBottom: "1px solid ".concat(theme.palette.divider),
      '& > div': {
        '& > *:first-child': {
          marginRight: 'auto'
        },
        '& > hr': {
          margin: theme.spacing(0, 1)
        }
      }
    },
    item: {
      minHeight: theme.spacing(6),
      paddingLeft: function paddingLeft() {
        var _ref12 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
            _ref12$level = _ref12.level,
            level = _ref12$level === void 0 ? 0 : _ref12$level;

        return theme.spacing(level * 1.5);
      },
      '& div[role=icon]': {
        minWidth: theme.spacing(4)
      },
      '&.dragging': {
        background: theme.palette.background.paper,
        border: "1px solid ".concat(theme.palette.divider),
        borderRadius: theme.shape.borderRadius,
        opacity: 0.6
      },
      '& > div[role=action]': {
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
        height: '100%',
        '& > button, & > *[role=dnd]': {
          opacity: theme.palette.action.disabledOpacity,
          '&:hover': {
            opacity: 1
          }
        }
      },
      '& + .entered': {
        height: 'fit-content !important',
        minHeight: 'fit-content !important'
      }
    }
  };
}); //* Components

function ElementItem(_ref13) {
  var prefix = _ref13.prefix,
      _ref13$level = _ref13.level,
      level = _ref13$level === void 0 ? 0 : _ref13$level,
      widget = _ref13.widget;
  var superior = widget.superior,
      uid = widget.uid,
      index = widget.index,
      importBy = widget.importBy,
      description = widget.description;

  var _useLocales = (0, _locales.useLocales)(),
      dt = _useLocales.getFixedT;

  var _useWidgetContext2 = (0, _customs2.useWidgetContext)(),
      definitions = _useWidgetContext2.definitions;

  var _useContext = (0, _react.useContext)(NodeContext),
      container = _useContext.container,
      dnd = _useContext.dnd,
      expandeds = _useContext.expandeds,
      onActived = _useContext.onActived,
      onAppend = _useContext.onAppend,
      onDestroy = _useContext.onDestroy,
      onModify = _useContext.onModify,
      onElementChange = _useContext.onElementChange;

  var substratums = (0, _customs2.useSubstratumWidgets)({
    superior: uid,
    stringify: false
  });
  var appendable = isAppendable(importBy, definitions, substratums);
  var dragging = Boolean(dnd.drag);
  var draggable = Boolean(dnd.enabled && (!superior || superior.endsWith('.children')));
  var isExpanded = expandeds.has(uid) || dnd.enabled;
  var classes = useStyles({
    level: level
  });
  (0, _react.useEffect)(function () {
    var i = container().indexOf(uid);

    if (i !== index) {
      onModify(_objectSpread(_objectSpread({}, widget), {}, {
        index: i
      }));
    }
  }, [widget]);
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_reactBeautifulDnd.Draggable, {
    index: index,
    draggableId: uid,
    isDragDisabled: !draggable
  }, function (dragProv) {
    return /*#__PURE__*/_react["default"].createElement(_ListItem["default"], {
      button: true,
      classes: {
        container: (0, _clsx["default"])(classes.item, {
          dragging: dnd.drag === uid
        })
      },
      onClick: function onClick() {
        return onActived(uid);
      },
      ContainerProps: _objectSpread(_objectSpread({}, dragProv.draggableProps), {}, {
        ref: dragProv.innerRef,
        'data-widget': uid
      })
    }, /*#__PURE__*/_react["default"].createElement(_ListItemIcon["default"], {
      role: "icon",
      onClick: function onClick(e) {
        return e.stopPropagation();
      }
    }, Object.keys(substratums).length > 0 && /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
      size: "small",
      onClick: function onClick() {
        return onElementChange(uid, !isExpanded);
      }
    }, isExpanded ? /*#__PURE__*/_react["default"].createElement(_ExpandMore["default"], null) : /*#__PURE__*/_react["default"].createElement(_ChevronRight["default"], null))), /*#__PURE__*/_react["default"].createElement(_ListItemText["default"], prefix ? {
      primary: prefix,
      secondary: description
    } : {
      primary: description
    }), /*#__PURE__*/_react["default"].createElement(_ListItemSecondaryAction["default"], _extends({}, dragProv.dragHandleProps, {
      role: "action"
    }), !dnd.enabled && /*#__PURE__*/_react["default"].createElement(_iconMenuButton["default"], {
      size: "small",
      color: "primary",
      icon: /*#__PURE__*/_react["default"].createElement(_SettingsOutlined["default"], null)
    }, appendable && /*#__PURE__*/_react["default"].createElement(_iconMenuButton.IconMenuItem, {
      icon: /*#__PURE__*/_react["default"].createElement(_Add["default"], {
        color: "primary"
      }),
      text: dt('btn-create-element'),
      onClick: function onClick() {
        onElementChange(uid, true);
        onAppend({
          target: uid,
          value: 'children'
        });
      }
    }), /*#__PURE__*/_react["default"].createElement(_iconMenuButton.IconMenuItem, {
      icon: /*#__PURE__*/_react["default"].createElement(_Close["default"], {
        color: "secondary"
      }),
      text: dt('btn-remove-element'),
      onClick: function onClick() {
        return onDestroy(uid);
      }
    })), dnd.enabled && draggable && (!dragging || dnd.drag === uid) && /*#__PURE__*/_react["default"].createElement(_ImportExport["default"], {
      role: "dnd"
    })));
  }), /*#__PURE__*/_react["default"].createElement(_Collapse["default"], {
    "in": isExpanded,
    timeout: "auto",
    classes: {
      entered: 'entered'
    }
  }, isExpanded && Object.entries(substratums).reduce(function (result, _ref14) {
    var _ref15 = _slicedToArray(_ref14, 2),
        propName = _ref15[0],
        substratum = _ref15[1];

    return result.concat(substratum === null || substratum === void 0 ? void 0 : substratum.map(function ($widget) {
      return /*#__PURE__*/_react["default"].createElement(ElementItem, {
        key: $widget.uid,
        prefix: propName !== 'children' && propName,
        level: level + 1,
        widget: $widget
      });
    }));
  }, [])));
}

function ElementStructure(_ref16) {
  var open = _ref16.open,
      onActived = _ref16.onActived,
      onAppend = _ref16.onAppend,
      onDestroy = _ref16.onDestroy,
      onModify = _ref16.onModify,
      onReadyEdit = _ref16.onReadyEdit,
      onSort = _ref16.onSort;

  var _useLocales2 = (0, _locales.useLocales)(),
      dt = _useLocales2.getFixedT;

  var _useWidgetContext3 = (0, _customs2.useWidgetContext)(),
      widgets = _useWidgetContext3.widgets,
      onListenersActived = _useWidgetContext3.onListenersActived;

  var _useSubstratumWidgets = (0, _customs2.useSubstratumWidgets)({
    stringify: false
  }),
      substratum = _useSubstratumWidgets.children;

  var _useDnd = useDnd(widgets, onSort),
      _useDnd2 = _slicedToArray(_useDnd, 3),
      dnd = _useDnd2[0],
      dispatch = _useDnd2[1],
      handleDnd = _useDnd2[2];

  var appbarRef = (0, _react.useRef)();
  var classes = useStyles();

  var _useNodeProviderValue = useNodeProviderValue(appbarRef, dnd, dispatch, onActived, onAppend, onDestroy, onModify),
      _useNodeProviderValue2 = _slicedToArray(_useNodeProviderValue, 2),
      nodeValue = _useNodeProviderValue2[0],
      setExpandeds = _useNodeProviderValue2[1];

  (0, _react.useEffect)(function () {
    if (open) {
      onListenersActived(false);
    }
  }, [open]);
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_AppBar["default"], {
    ref: appbarRef,
    position: "static",
    color: "inherit",
    className: classes.appbar,
    elevation: 0
  }, /*#__PURE__*/_react["default"].createElement(_Toolbar["default"], {
    variant: "dense"
  }, /*#__PURE__*/_react["default"].createElement(_Typography["default"], {
    variant: "subtitle1",
    color: "primary"
  }, dt('ttl-elements')), /*#__PURE__*/_react["default"].createElement(_Tooltip["default"], {
    title: dt('btn-edit-ready-handle')
  }, /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
    size: "small",
    color: "primary",
    onClick: onReadyEdit
  }, /*#__PURE__*/_react["default"].createElement(_QueuePlayNext["default"], null))), /*#__PURE__*/_react["default"].createElement(_Tooltip["default"], {
    title: dt('btn-create-element')
  }, /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
    size: "small",
    color: "primary",
    onClick: function onClick() {
      return nodeValue.onAppend();
    }
  }, /*#__PURE__*/_react["default"].createElement(_Add["default"], null))), /*#__PURE__*/_react["default"].createElement(_Divider["default"], {
    flexItem: true,
    orientation: "vertical"
  }), /*#__PURE__*/_react["default"].createElement(_Switch["default"], {
    color: "secondary",
    size: "small",
    checked: dnd.enabled,
    onChange: function onChange(_ref17) {
      var checked = _ref17.target.checked;
      dispatch({
        type: 'switch',
        value: checked
      });
      setExpandeds(new Set());
    }
  }))), /*#__PURE__*/_react["default"].createElement(_reactBeautifulDnd.DragDropContext, {
    onDragStart: handleDnd.start,
    onDragUpdate: handleDnd.update,
    onDragEnd: handleDnd.end
  }, /*#__PURE__*/_react["default"].createElement(_reactBeautifulDnd.Droppable, {
    isCombineEnabled: true,
    direction: "vertical",
    droppableId: ROOT_ID,
    isDropDisabled: !dnd.enabled
  }, function (dropProv) {
    return /*#__PURE__*/_react["default"].createElement(NodeContext.Provider, {
      value: nodeValue
    }, /*#__PURE__*/_react["default"].createElement(_List["default"], _extends({}, dropProv.droppableProps, {
      ref: dropProv.innerRef,
      role: "element",
      className: classes.root
    }), substratum === null || substratum === void 0 ? void 0 : substratum.map(function (widget) {
      return /*#__PURE__*/_react["default"].createElement(ElementItem, {
        key: widget.uid,
        widget: widget
      });
    }), dropProv.placeholder));
  })));
}