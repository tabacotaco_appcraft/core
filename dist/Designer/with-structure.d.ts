export function useOverrided(PropElement: any, category: any, controlProps: any): any[];
export default function withStructure(category: any, PropElement: any): {
    (controlProps: any): any;
    Naked: any;
    displayName: string;
};
