import React from 'react';
import PropTypes from 'prop-types';
import { AppcraftParser } from '@appcraft/parser';
import { DefinitionsType, GlobalStateType, ReadyTodoType, StateBindingType, WidgetOptionsType } from '../Visualizer';
declare const PROP_TYPES: {
    actions: PropTypes.Requireable<PropTypes.ReactElementLike>;
    definitions: PropTypes.Validator<object>;
    value: PropTypes.Requireable<object>;
    widgetProxy: PropTypes.Requireable<{
        [x: string]: PropTypes.ReactComponentLike;
    }>;
    onCancel: PropTypes.Requireable<(...args: any[]) => any>;
    onConfirm: PropTypes.Requireable<(...args: any[]) => any>;
    onJsonModeOpen: PropTypes.Requireable<(...args: any[]) => any>;
    overrideMixedOptions: PropTypes.Requireable<(...args: any[]) => any>;
    overridePropControl: PropTypes.Requireable<(...args: any[]) => any>;
    InputStyles: PropTypes.Requireable<Required<PropTypes.InferProps<{
        color: PropTypes.Requireable<"primary" | "secondary">;
        margin: PropTypes.Requireable<"normal" | "none" | "dense">;
        size: PropTypes.Requireable<"small" | "medium">;
        variant: PropTypes.Requireable<"standard" | "filled" | "outlined">;
    }>>>;
    classes: PropTypes.Requireable<Required<PropTypes.InferProps<{
        root: PropTypes.Requireable<string>;
        header: PropTypes.Requireable<string>;
        drawer: PropTypes.Requireable<string>;
        footer: PropTypes.Requireable<string>;
        required: PropTypes.Requireable<string>;
        structure: PropTypes.Requireable<string>;
    }>>>;
};
declare namespace AppcraftDesigner {
    namespace def {
        type DesignerValue = {
            subject?: string;
            ready?: ReadyTodoType;
            state?: GlobalStateType;
            widgets?: WidgetOptionsType[];
        };
        interface OverrideControlEvent<T extends AppcraftParser.def.PropType> extends Omit<AppcraftParser.def.PropDefinition<T>, 'options' | 'uid'> {
            pathname: string;
            propName: string;
        }
        interface OverrideMixedOptionEvent<T extends AppcraftParser.def.PropType> {
            pathname: string;
            options: AppcraftParser.def.PropDefinition<T>[];
        }
        export interface Props extends Pick<PropTypes.InferProps<typeof PROP_TYPES>, 'InputStyles' | 'actions' | 'classes' | 'widgetProxy'> {
            definitions: DefinitionsType;
            value?: DesignerValue;
            onCancel?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
            onConfirm?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, newValue: DesignerValue) => void;
            overrideMixedOptions?: <T extends AppcraftParser.def.PropType>(e: OverrideMixedOptionEvent<T>) => AppcraftParser.def.PropDefinition<T>[];
            overridePropControl?: <T extends AppcraftParser.def.PropType>(e: OverrideControlEvent<T>) => React.ElementType | null;
            onJsonModeOpen?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, props: WidgetOptionsType['props'], confirm: (props: WidgetOptionsType['props']) => void) => void;
        }
        export {};
    }
    namespace hooks {
        type ControlParam = {
            type: symbol;
            target?: string;
            value?: any;
            options?: any;
        };
        type ControlState = {
            actived?: string;
            listeners: string[];
            subject: string;
            ready: ReadyTodoType;
            state: GlobalStateType;
            widgets: WidgetOptionsType[];
        };
        export type ControlValue = [Record<string, symbol>, ControlState, (e: ControlParam | ControlParam[]) => void];
        export interface EditorParam extends WidgetOptionsType {
            superior?: string;
            description: string;
        }
        export type ElementParam = {
            type: 'SET_STATE' | 'WIDGET_APPEND' | 'WIDGET_DESTROY';
            target?: string;
            value?: any;
            options?: any;
        };
        export interface BindingParam extends StateBindingType {
            uid: string;
        }
        export {};
    }
}
declare const _default: React.ForwardRefExoticComponent<AppcraftDesigner.def.Props & {
    lang?: string;
    locales?: {
        [lang: string]: {
            [code: string]: string;
        };
    };
} & React.RefAttributes<any>>;
export default _default;
