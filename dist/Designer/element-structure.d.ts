export default function ElementStructure({ open, onActived, onAppend, onDestroy, onModify, onReadyEdit, onSort }: {
    open: any;
    onActived: any;
    onAppend: any;
    onDestroy: any;
    onModify: any;
    onReadyEdit: any;
    onSort: any;
}): JSX.Element;
