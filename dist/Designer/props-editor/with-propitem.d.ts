export default function withPropitem(category: any, PropElement: any): {
    (controlProps: any): JSX.Element;
    Naked: any;
    displayName: string;
};
