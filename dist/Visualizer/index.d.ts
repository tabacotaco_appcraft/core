import React from 'react';
import PropTypes from 'prop-types';
import { AppcraftParser } from '@appcraft/parser';
declare const PROP_TYPES: {
    IMPL: {
        lazyDeps: PropTypes.Requireable<any[]>;
        ready: PropTypes.Requireable<object[]>;
    };
    MAIN: {
        children: PropTypes.Validator<PropTypes.ReactNodeLike>;
        proxy: PropTypes.Requireable<{
            [x: string]: PropTypes.ReactComponentLike;
        }>;
        ready: PropTypes.Requireable<object[]>;
        definitions: PropTypes.Requireable<Required<PropTypes.InferProps<{
            decorations: PropTypes.Requireable<{
                [x: string]: Required<PropTypes.InferProps<{
                    configTypes: PropTypes.Requireable<object>;
                    defaultProps: PropTypes.Requireable<object>;
                    description: PropTypes.Requireable<string>;
                    propTypes: PropTypes.Requireable<object>;
                }>>;
            }>;
            props: PropTypes.Requireable<{
                [x: string]: Required<PropTypes.InferProps<{
                    defaultProps: PropTypes.Requireable<object>;
                    description: PropTypes.Requireable<string>;
                    propTypes: PropTypes.Requireable<object>;
                }>>;
            }>;
        }>>>;
        state: PropTypes.Requireable<{
            [x: string]: Required<PropTypes.InferProps<{
                path: PropTypes.Validator<string>;
                typeId: PropTypes.Validator<string>;
                defaultValue: PropTypes.Requireable<any>;
            }>>;
        }>;
        widgets: PropTypes.Requireable<Required<PropTypes.InferProps<{
            description: PropTypes.Requireable<string>;
            handles: PropTypes.Requireable<object[]>;
            importBy: PropTypes.Requireable<string>;
            index: PropTypes.Requireable<number>;
            props: PropTypes.Requireable<object>;
            superior: PropTypes.Requireable<string>;
            typePairs: PropTypes.Requireable<{
                [x: string]: string;
            }>;
            uid: PropTypes.Validator<string>;
            decoration: PropTypes.Requireable<Required<PropTypes.InferProps<{
                description: PropTypes.Requireable<string>;
                importBy: PropTypes.Requireable<string>;
                options: PropTypes.Requireable<object>;
                typePairs: PropTypes.Requireable<{
                    [x: string]: string;
                }>;
                uid: PropTypes.Validator<string>;
            }>>[]>;
        }>>[]>;
    };
};
declare namespace AppcraftVisualizer {
    namespace def {
        enum BaseTypeName {
            Array = 0,
            Boolean = 1,
            Date = 2,
            Number = 3,
            Object = 4,
            String = 5
        }
        enum RefTypeName {
            input = 0,
            state = 1,
            todo = 2
        }
        export type StateBinding = {
            path: string;
            typeId: string;
            defaultValue?: any;
        };
        type VariableType = keyof typeof BaseTypeName | keyof typeof RefTypeName;
        interface Variable<T extends VariableType = VariableType> {
            uid: string;
            type: T;
            finalType?: BaseTypeName;
            description: string;
            initValue?: Variable[] | Record<string, Variable> | string;
            treatments?: ({
                uid: string;
                description: string;
                name: string;
                args?: Variable[];
            })[];
        }
        type Condition = {
            uid: string;
            description: string;
            source: Variable;
            value: Variable;
        };
        interface HandleBase {
            type: string;
            uid: string;
            description: string;
            state?: string;
            condition?: Condition[];
        }
        interface RequestHandle extends HandleBase {
            type: 'request';
            url: string;
            method: 'DELETE' | 'GET' | 'HEAD' | 'OPTIONS' | 'PATCH' | 'POST' | 'PUT';
            header?: Record<string, string>;
            search?: Record<string, Variable>;
            body?: Variable<'Object'>;
        }
        interface CalculatorHandle extends HandleBase {
            type: 'calculator';
            params: Variable[];
            template: string;
        }
        interface MapHandle extends HandleBase {
            type: 'map';
            mappable?: Condition[];
            source: Variable<'Array'>[];
            pairs: (Omit<CalculatorHandle, 'condition'> & {
                path: string;
            })[];
        }
        interface OptionsBase {
            description?: string;
            importBy?: string;
            typePairs?: Record<string, string>;
            uid: string;
        }
        export interface WidgetOptions extends OptionsBase {
            decoration?: (OptionsBase & {
                options: Record<string, any>;
            })[];
            handles: Record<string, (RequestHandle | CalculatorHandle | MapHandle)[]>;
            index: number;
            props: Record<string, any>;
            superior?: string;
        }
        export interface ImplementProps extends Pick<PropTypes.InferProps<typeof PROP_TYPES.IMPL>, 'lazyDeps'> {
            ready?: (RequestHandle | CalculatorHandle | MapHandle)[];
        }
        export interface MainProps extends Pick<PropTypes.InferProps<typeof PROP_TYPES.MAIN>, 'children' | 'proxy'> {
            ready?: ImplementProps['ready'];
            state?: Record<string, StateBinding[]>;
            widgets: WidgetOptions[];
            definitions?: {
                decorations?: Record<string, {
                    description?: string;
                    propTypes?: AppcraftParser.def.PropDefinition<'arrayOf'>;
                    configTypes?: AppcraftParser.def.PropDefinition<'arrayOf'>;
                    defaultProps?: Record<string, any>;
                }>;
                props?: Record<string, {
                    description?: string;
                    propTypes: AppcraftParser.def.PropDefinition<'exact'>;
                    defaultProps?: Record<string, any>;
                }>;
            };
        }
        export {};
    }
}
export declare type DefinitionsType = AppcraftVisualizer.def.MainProps['definitions'];
export declare type GlobalStateType = AppcraftVisualizer.def.MainProps['state'];
export declare type ReadyTodoType = AppcraftVisualizer.def.MainProps['ready'];
export declare type StateBindingType = AppcraftVisualizer.def.StateBinding;
export declare type WidgetOptionsType = AppcraftVisualizer.def.WidgetOptions;
export declare const WidgetImplement: React.FC<AppcraftVisualizer.def.ImplementProps>;
export declare const WidgetWrapper: React.FC<AppcraftVisualizer.def.MainProps>;
declare const Visualizer: React.FC<Pick<AppcraftVisualizer.def.MainProps, 'proxy' | 'ready' | 'state' | 'widgets'>>;
export default Visualizer;
